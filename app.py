from fastapi import FastAPI, UploadFile, File
import aiofiles


app = FastAPI()

@app.post("/file/")
async def create_upload_file(file: UploadFile = File(...)):
    try:
        async with aiofiles.open(f"{file.filename}", "wb") as f:
            core = await file.read()
            await f.write(core)
    except Exception:
        return {"message": "There was an error uploading the file"}
    return {"filename": file.filename}
